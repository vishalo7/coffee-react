import React ,{Suspense} from 'react';
// import logo from './logo.svg';
import './App.css';
import './index.css';
// import { Index } from './containers';
const Index = React.lazy(() => import('./containers'));
// import Coffe from './containers/component/home'
// import { Index } from './index';


function App() {
  return (
    <div className="App">
      <div className="auth-wrapper">
        <div className="auth-inner">
        <Suspense fallback={<div>Loading...</div>}>
            <Index />
      </Suspense></div></div>
            
    </div>
  );
}


export default App;

// import React from "react";

// import {
//   Link,
//   // DirectLink,
//   Element,
//   Events,
//   animateScroll,
//   scrollSpy,
//   scroller
// } from "react-scroll";

// const durationFn = function (deltaTop: any) {
//   return deltaTop;
// };

// class Section extends React.Component {
//   // constructor(props: any) {
//   //   super(props);
//   //   this.scrollToTop = this.scrollToTop.bind(this);
//   // }

//   // componentDidMount() {
//   //   Events.scrollEvent.register("begin", function () {
//   //     console.log("begin", arguments);
//   //   });

//   //   Events.scrollEvent.register("end", function () {
//   //     console.log("end", arguments);
//   //   });
//   // }
//   // scrollToTop() {
//   //   // scroll.scrollToTop();
//   // }
//   // scrollTo(offset:any) {
//   //   scroller.scrollTo("scroll-to-element", {
//   //     duration: 800,
//   //     delay: 0,
//   //     smooth: "easeInOutQuart",
//   //     offset: offset
//   //   });
//   // }
//   // scrollToWithContainer() {
//   //   let goToContainer = new Promise((resolve, reject) => {
//   //     Events.scrollEvent.register("end", () => {
//   //       resolve();
//   //       Events.scrollEvent.remove("end");
//   //     });

//   //     scroller.scrollTo("scroll-container", {
//   //       duration: 800,
//   //       delay: 0,
//   //       smooth: "easeInOutQuart"
//   //     });
//   //   });

//   //   goToContainer.then(() =>
//   //     scroller.scrollTo("scroll-container-second-element", {
//   //       duration: 800,
//   //       delay: 0,
//   //       smooth: "easeInOutQuart",
//   //       containerId: "scroll-container",
//   //       offset: 50
//   //     })
//   //   );
//   // }
//   // componentWillUnmount() {
//   //   Events.scrollEvent.remove("begin");
//   //   Events.scrollEvent.remove("end");
//   // }
//   render() {
//     return (
//       <div>

//         <Link
//           activeClass="active"
//           to="firstInsideContainer"
//           spy={true}
//           smooth={true}
//           duration={250}
//           containerId="containerElement"
//           style={{ display: "inline-block", margin: "20px" }}
//         >
//           Go to first element inside container
//         </Link>

//         <Link
//           activeClass="active"
//           to="secondInsideContainer"
//           spy={true}
//           smooth={true}
//           duration={250}
//           containerId="containerElement"
//           style={{ display: "inline-block", margin: "20px" }}
//         >
//           Go to second element inside container
//         </Link>

//         <Element
//           name="test7"
//           className="element"
//           id="containerElement"
//           style={{
//             position: "relative",
//             height: "200px",
//             overflow: "scroll",
//             marginBottom: "100px"
//           }}
//         >
//           <Element name="firstInsideContainer" style={{ marginBottom: "200px"}}>
//             first element inside container asdfasd
//           </Element>

//           <Element  name="secondInsideContainer"  style={{  marginBottom: "200px" }}>
//             second element inside container
//           </Element>
//         </Element>
//         {/* <div
//           className="element"
//           id="scroll-container"
//           style={{
//             height: "200px",
//             overflow: "scroll",
//             marginBottom: "100px"
//           }}
//         >
//           <a onClick={this.scrollToTop}>To the top!</a> */}
//       </div>
//     );
//   }
// }
// export default Section;
