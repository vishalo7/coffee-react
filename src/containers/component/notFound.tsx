import React from 'react'
import { Link } from 'react-router-dom';


import NotFoundPage from '../../assets/img/404.jpg';

interface Props {

}

export const NotFound = ({ }: Props) => {
    return (
        <div style={{ marginTop: "240px" }}>
            <img src={NotFoundPage} style={{ width: 400, height: 400, display: 'block', margin: 'auto', position: 'relative' }} />
            <div style={{ display: "flex", justifyContent: "center",  alignItems: "center" }}>
                <Link to="/">Return to Home Page</Link>
            </div>
        </div>
    );
}
