import React from 'react'
import {ROUTES_LIST} from '../routes';
import { BrowserRouter as Router, Switch } from "react-router-dom";
import { PrivateRoutes } from '../utils/routes-utils';

interface Props {

}

const Index: React.FC<Props> = () => { 
    return (
        <Router>
            <Switch>
                {ROUTES_LIST.map((key:any, index:any) => (<PrivateRoutes key={index} {...key} />))}
            </Switch>
        </Router>
    )
}
export default Index