import home from './containers/component/home';
import { Generic } from './containers/component/generic';
import { NotFound } from './containers/component/notFound';
import Login from './containers/auth/login';

const ROUTES = {
    container: {
        generic: 'generic'
    },

    generic: {
        view: 'generic'
    }
};

export const ROUTES_LIST = [
    {
        exact: true,
        NotFound: true,
        path: "/",
        component: home
    },
    {
        NotFound: true,
        path: "/login",
        component: Login
    },
    {
        NotFound: true,
        path: "/generic",
        component: Generic
    },
    {
        NotFound: false,
        component: NotFound
    },
]