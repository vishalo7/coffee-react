import React from 'react';
import { Route } from 'react-router';
import Header from '../containers/header';
import Footer from '../containers/footer';

interface Props {
    NotFound:any,
    exact: boolean,
    path: string,
    component: any
}

export const PrivateRoutes = (Props: Props) => {
console.log(Props)
    return (
        <div>
           {/* {Props.NotFound && <Header />}  */}
            <Route  {...Props} />
            {/* {Props.NotFound && <Footer />} */}
        </div>
    )
}
